#!/usr/bin/python

import sys
import configparser
import subprocess as s
import json
from time import localtime, strftime

config = configparser.ConfigParser()
config.read("/etc/srvChk/config.ini")

res = {"lastUpdate": strftime("%a, %d %b %Y %H:%M:%S", localtime())}
srv = []
for service in config.sections():
    cfg = config[service]

    args = cfg["cmd"].split()
    for key in cfg:
        if key == "cmd": continue
        args.append("-" + str(key))
        args.append(str(cfg[key]))

    #print("Calling", args)
    retval = s.call(args, stdout=sys.stderr)
    state = "up" if retval == 0 else "down"
    
    srv.append({"service": service, "status": state, "retval": retval})

res["services"] = srv
print(json.dumps(res, sort_keys=True, indent=4))
